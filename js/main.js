// Navs

$(function() {
    var pull = $('.navbar-toggle');
    var menu = $(pull.attr("data-target"));

    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.slideToggle(100);
    });

    $(window).resize(function(){
        var w = $(window).width();
        if(w > 768 && menu.is(':hidden')) {
            menu.removeAttr('style');
        }
    });
});



$(".btn-modal").fancybox({
    'padding'    : 0,
    'maxWidth'   : 600
});

$(".btn-modal-md").fancybox({
    'padding'    : 0,
    'maxWidth'   : 830
});



// tabs

$('.sh-table tr').click(function(e) {
    e.preventDefault();
    var box = $(this).closest('.sh-table');

    box.find('tr').removeClass('active');
    $(this).addClass('active');
});


// tabs nav

$('.navbar > li > a').click(function(e) {
    e.preventDefault();
    var tab = $($(this).attr("data-target"));
    var box = $(this).closest('body');

    $(this).closest('.navbar').find('li').removeClass('active');
    $(this).closest('li').addClass('active');

    box.find('.page-item').removeClass('active');
    box.find(tab).addClass('active');
});
